/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labexam2;

/**
 *
 * @author Akshay Reddy Vontari
 */
public class Teacher extends Person{
    private int numCourses;
    private String[] courses;
    private final int MAX_COURSES=5;

    public Teacher(String name, String address) {
        super(name, address);
        this.numCourses = 0;
        this.courses = new String[MAX_COURSES];
    }

    @Override
    public String toString() {
        return "Teacher :"+super.toString();
    }
    
    public boolean addCourse(String course){

       boolean a;
       
        for(String e: courses){
        if(e != course){
        courses[numCourses]=e;
        
        }
        numCourses = numCourses + 1;
    }
        if(this.numCourses != numCourses){
            a = true;
        }
        else{
            a=false;
        }
       return a;
    }
    
    
    
    
    public boolean removeCourse(String course){
        boolean b;
        for(String s:courses){
            if(s == course){
             courses[MAX_COURSES] = s;
            }
            numCourses = numCourses - 1;
        }
        if(this.numCourses == numCourses){
             b = true;
        }
        else{
            b=false;
        }
        
       return b;
        
    }
    
    
    
    
    
}
