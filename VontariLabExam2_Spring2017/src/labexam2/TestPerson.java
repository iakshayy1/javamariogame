/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labexam2;

/**
 *
 * @author Akshay Reddy Vontari
 */
public class TestPerson {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Student s1 = new Student("Bill Gates","1 Happy Ave.");
        s1.addCoursegrade("CS101", 97);
        s1.addCoursegrade("CS102", 68);
        System.out.print(s1);
        s1.printGrades();
        
        System.out.println("Average is "+s1.getAverageGrade());
        Teacher t1 = new Teacher("Ada Lovelace","8 Sunset Way");
        System.out.println(t1.toString());
        String[] courses = {"CS101","CS102","CS103"};
        for(int i = 0;i<courses.length;i++){
        t1.addCourse(courses[i]);
        if(t1.addCourse(courses[i])==true){
            System.out.println(courses[i]+" added");
        }
        else
        {
            System.out.println(courses[i]+" cannot be added");
        }
        t1.removeCourse(courses[i]);
        if(t1.removeCourse(courses[i])==true){
            System.out.println(courses[i]+" removed");
        }
        else
        {
            System.out.println(courses[i]+ " cannot be removed");
        }
        
        
    }
    }
}
    

