/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labexam2;

import java.util.ArrayList;

/**
 *
 * @author Akshay Reddy Vontari
 */
public class Student extends Person{
    private int numCourses;
    private ArrayList<String> courses = new ArrayList<String>();
    private ArrayList<Integer> grades = new ArrayList<Integer>();
    private final int MAX_COURSES=30;

    public Student() {
    }

    public Student(String name, String address) {
        super(name, address);
        this.numCourses=0;
        this.courses = new ArrayList<String>();
        this.grades = new ArrayList<Integer>();
    }

    @Override
    public String toString() {
        return "Student :"+super.toString();
    }
    
    public void addCoursegrade(String course,int grade){
        courses.add(course);
        grades.add(grade);
        numCourses++;
    }
    
    public void printGrades(){
        for(int i = 0;i<courses.size();i++){
            System.out.println(" "+courses.get(i)+":"+grades.get(i));
        }
    }
    
    public double getAverageGrade(){
        int grde=0;
        for(int j=0;j<grades.size();j++){
            grde = grde + grades.get(j);
        }
       
        return (double)grde/courses.size();
    }
    
    
    
    
    
}
