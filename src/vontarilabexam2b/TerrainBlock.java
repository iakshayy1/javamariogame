/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vontarilabexam2b;

/**
 *
 * @author Akshay Reddy Vontari
 */
public class TerrainBlock  {
    private boolean starman;
    private String gameCharacter;

    public TerrainBlock(boolean starman, String gameCharacter) {
        this.starman = starman;
        this.gameCharacter = gameCharacter;
    }
    
    public int getTerrainPoints(){
        if(starman = true){
            return 100;
        }
        else
        {
            return 10;
        } 
           
    }
    
    public int getPoints(){
        if((gameCharacter != null)){
            return getPoints();        }
        else
        {
            return getTerrainPoints();
        }
    }
    
    public boolean containsEnemy(){
        if(gameCharacter == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean isStarman() {
        return starman;
    }

    public String getGameCharacter() {
        return gameCharacter;
    }

    public void setGameCharacter(String gameCharacter) {
        this.gameCharacter = gameCharacter;
    }
    
    
}
