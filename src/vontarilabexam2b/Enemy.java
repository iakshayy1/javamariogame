/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vontarilabexam2b;

/**
 *
 * @author Akshay Reddy Vontari
 */
public class Enemy extends GameCharacter{

    public Enemy(int life, String name, int points) {
        super(life, name, points);
    }
    
    public String printState(){
        if(super.getLife()<1){
            return super.getName()+" dies";
        }
        else
        {
            return super.getName()+ " Lives";
        }
            
    }
    
    
    
    
}
